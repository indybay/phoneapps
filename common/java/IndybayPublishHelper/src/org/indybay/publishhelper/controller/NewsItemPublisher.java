/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.controller;

import java.util.Hashtable;
import java.util.Vector;

import org.indybay.publishhelper.model.IndyPreferences;
import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.persistence.WebHelper;

/**
 * NewsItemPublisher is the main controller class that POSTs NewsItems
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class NewsItemPublisher {

	/**
	 * Preferences object containing base url and what to do afetr publish
	 * occurs
	 */
	private IndyPreferences preferences = null;

	/**
	 * 
	 */
	private WebHelper webHelper = null;

	/**
	 * Constructor requires a reference to a preference object that keeps tract
	 * of where to post, what to do after posting etc...
	 */
	public NewsItemPublisher(final IndyPreferences newPreferences,
			final WebHelper newWebHelper) {
		this.preferences = newPreferences;
		this.webHelper = newWebHelper;
	}

	/**
	 * Main publish action that POSTs to Indybay
	 * 
	 * @param newsItem
	 * @throws CaptchaException
	 * @throws ConnectionException
	 * @throws ServerSidePublishException
	 * @throws ConfigurationException
	 */
	public Vector publishNewsItem(final NewsItem newsItem) throws Exception {

		final Vector validationProblems = NewsItemValidator
				.validateNewsItem(newsItem);
		if (validationProblems.size() > 0) {
			return validationProblems;
		}

		final Hashtable postFields = this.prepareFields(newsItem);
		// try {

		final String results = this.webHelper.httpPOST(this.preferences
				.getPublishPageURL(), postFields, newsItem.getAttachments());
		final String resultURL = this.getURLFromResultString(results);
		final Integer newsItemId = this.getNewsItemIdFromURL(resultURL);
		newsItem.setId(newsItemId);
		newsItem.setPostedURL(resultURL);
		newsItem.setPublishDate(new java.util.Date());
		return null;
		// } catch (final Exception e) {
		// throw new ConnectionException("Error posting data to server: " + e);
		// }
	}

	/**
	 * Given a URL of a result after POSTing, this method returns the id portion
	 * of the url
	 * 
	 * @param resultURL
	 * @return
	 * @throws ServerSidePublishException
	 */
	private Integer getNewsItemIdFromURL(final String resultURL)
			throws Exception {
		if (resultURL == null) {
			throw new RuntimeException(
					"getNewsItemIdFromURL was handed NULL URL");
		}
		int i = -1;
		int ii = resultURL.indexOf("/");
		while (ii > -1) {
			i = ii;
			ii = resultURL.indexOf("/", ii + 1);
		}
		if (i < 0) {
			throw new Exception("Returned URL was in an unexpected format");
		}
		final int j = resultURL.indexOf(".php", i);
		if ((j < 0) || (j - i > 100)) {
			throw new Exception("Returned URL was not for a php page");
		}
		int k = 0;
		try {
			k = Integer.parseInt(resultURL.substring(i + 1, j));
		} catch (final Exception e) {
			throw new Exception("ID in returned URL was non-numeric: "
					+ resultURL.substring(i + 1, j));
		}
		return new Integer(k);
	}

	/**
	 * Parses a result page and returns the url of the just posted item
	 * 
	 * @param resultString
	 * @return url
	 * @throws ServerSidePublishException
	 */
	private String getURLFromResultString(final String resultString)
			throws Exception {
		if (resultString == null) {
			throw new RuntimeException(
					"getURLFromResultString was handed NULL result string");
		}

		int i = resultString.indexOf("SUCCESS");
		if (i < 0) {
			System.out.println(resultString);
			throw new Exception(
					"Publish failed to return result success page. Result Page Was "
							+ resultString);
		}
		i = resultString.indexOf("/newsitems/", i);
		if (i < 0) {
			throw new Exception(
					"Publish failed to return link to resulting post: "
							+ resultString);
		}
		final int j = resultString.indexOf(".php", i);
		if (j < 0) {
			throw new Exception(
					"Publish failed to well formatted link to resulting post");
		}
		String url = resultString.substring(i, j + 4);
		url = "http://" + this.preferences.getBaseURL() + url;

		return url;
	}

	/**
	 * Sets up a HashMap with name value pairs for the nonfile data to be posted
	 * 
	 * @param newsItem
	 * @return hashMap
	 * @throws ConnectionException
	 * @throws ConfigurationException
	 */
	private Hashtable prepareFields(final NewsItem newsItem) throws Exception {
		final Hashtable map = new Hashtable();
		map.put("title", newsItem.getTitle());
		map.put("author", newsItem.getAuthor());
		map.put("summary", newsItem.getSummary());
		map.put("text", newsItem.getText());
		map.put("related_url", newsItem.getRelatedLink());
		map.put("email", newsItem.getEmail());
		if (newsItem.isDisplayEmail()) {
			map.put("display_email", "1");
		} else {
			map.put("display_email", "");
		}
		map.put("region_id", String.valueOf(newsItem.getRegionId()));
		map.put("topic_id", String.valueOf(newsItem.getTopicId()));

		map.put("publish", "Publish");

		map.put("application_key", this.preferences.getApplicationKey());
		map.put("user_key", this.preferences.getPersonalKey());
		map.put("file_count", String.valueOf(newsItem.getAttachments().size()));
		return map;
	}

}
