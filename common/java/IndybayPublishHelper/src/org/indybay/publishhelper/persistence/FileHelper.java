/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Zogren
 * 
 */
public abstract class FileHelper extends IOHelper {

	/**
	 * Generates a unique filename of the given type
	 * 
	 * @param extension
	 * @return fileName
	 */
	public String generateUniqueFileName(final String extension) {
		String fileName = String.valueOf(System.currentTimeMillis());
		if (fileName.length() > 5) {
			fileName = fileName.substring(fileName.length() - 5, fileName
					.length());
		}
		fileName = "p" + fileName + extension;

		return fileName;
	}

	/**
	 * Returns a byte array given a file path
	 * 
	 * @param string
	 * @return bytes
	 * @throws IOException
	 */
	public byte[] getDataFromFilePath(final String string) throws IOException {
		final InputStream fis = this.getInputStreamFromFilePath(string);
		final byte[] data = new byte[fis.available()];
		fis.read(data);
		fis.close();
		return data;
	}

	/**
	 * Returns the file name portion of a file path
	 * 
	 * @param filePath
	 * @return fileName
	 */
	public String getFilenameFromFilePath(final String filePath) {
		int j = filePath.indexOf("/");
		int i = 0;
		while (j > -1) {
			i = j;
			j = filePath.indexOf("/", i + 1);
		}
		String fileName = null;
		if (i > 0) {
			fileName = filePath.substring(i + 1);
		} else if (i == 0) {
			fileName = filePath;
		}
		return fileName;
	}

	/**
	 * @param filePath
	 * @return
	 */
	protected abstract InputStream getInputStreamFromFilePath(String filePath)
			throws IOException;

	/**
	 * Way to copy data from file to outputstream
	 * 
	 * @param os
	 * @throws IOException
	 */
	protected void writeFileToOutputStream(final String filePath,
			final OutputStream os) throws IOException {
		final InputStream in = this.getInputStreamFromFilePath(filePath);
		this.copyFromInputToOutput(in, os);
	}
}
