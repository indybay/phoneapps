/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.persistence;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.indybay.publishhelper.model.NewsItemAttachment;

/**
 * Main class for web communications
 * 
 * @author Zogren for Indybay.org
 */
public abstract class WebHelper extends IOHelper {

	/**
	 * Some unique string is needed to divide the fields in a MIME type
	 */
	protected static final String HTTP_POST_BOUNDARY = "0194784892923";

	/**
	 * Performs an HTTP GET given a url as a String
	 * 
	 * @param url
	 * @return result of GET
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void httpBinaryGET(final String url, final OutputStream os)
			throws IOException {
		this.setupGETConnectionFromURL(url);
		final InputStream in = this.getInputStream();
		this.copyFromInputToOutput(in, os);
		in.close();
		this.cleanup();
	}

	/**
	 * Performs an HTTP GET given a url as a String
	 * 
	 * @param url
	 * @return result of GET
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public String httpGET(final String url) throws IOException {
		this.setupGETConnectionFromURL(url);
		final InputStream in = this.getInputStream();
		final String str = this.getStringFromInputStream(in);
		in.close();
		return str;
	}

	/**
	 * Performs an HTTP POST given a set of fields and a list of attached files
	 * to upload
	 * 
	 * @param url
	 * @param postFields
	 * @param attachments
	 * @return resulting HTML
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public String httpPOST(final String url, final Hashtable postFields,
			final Vector attachments) throws IOException {
		try {
			this.setupPOSTConnectionFromURL(url);
			final OutputStream os = this.getOutputStream();
			this.httpPOSTSend(os, postFields, attachments);
			final InputStream is = this.getInputStream();
			final String result = this.getStringFromInputStream(is);
			return result;
		} finally {
			this.cleanup();
		}
	}

	/**
	 * @throws IOException
	 */
	protected abstract void cleanup() throws IOException;

	/**
	 * @return
	 */
	protected abstract FileHelper getFileHelper();

	/**
	 * @return
	 * @throws IOException
	 */
	protected abstract InputStream getInputStream() throws IOException;

	/**
	 * @return
	 * @throws IOException
	 */
	protected abstract OutputStream getOutputStream() throws IOException;

	protected void httpPOSTSend(final OutputStream os,
			final Hashtable postFields, final Vector attachments)
			throws IOException {

		final DataOutputStream out = new DataOutputStream(os);

		final String httpPOSTDivider = "--" + WebHelper.HTTP_POST_BOUNDARY
				+ "\n";

		StringBuffer postContent = new StringBuffer("");
		final Enumeration iter = postFields.keys();
		String nextKey = null;
		String nextValue = null;
		while (iter.hasMoreElements()) {
			nextKey = (String) iter.nextElement();
			nextValue = (String) postFields.get(nextKey);
			postContent.append(httpPOSTDivider);
			postContent.append("Content-Disposition: form-data; name=\"");

			postContent.append(nextKey);
			postContent.append("\"\n\n");
			if (nextValue != null) {
				postContent.append(nextValue);
			}
			postContent.append("\n");
		}

		out.write(postContent.toString().getBytes());

		NewsItemAttachment nextAttachment = null;
		int j = 0;

		if (attachments != null) {
			final FileHelper fileHelper = this.getFileHelper();
			for (j = 0; j < attachments.size(); j++) {
				postContent = new StringBuffer("");
				nextAttachment = (NewsItemAttachment) attachments.elementAt(j);
				postContent.append(httpPOSTDivider);
				postContent.append("Content-Disposition: form-data; name=\"");
				postContent.append("linked_file_");
				postContent.append(String.valueOf(j + 1));
				postContent.append("\"; filename=\"");

				postContent.append(fileHelper
						.getFilenameFromFilePath(nextAttachment.getFilePath()));
				postContent
						.append("\"\nContent-Type: application/octet-stream\n\n");
				out.write(postContent.toString().getBytes());

				fileHelper.writeFileToOutputStream(
						nextAttachment.getFilePath(), out);

				out.write("\n".getBytes());
			}
		}
		postContent = new StringBuffer("");
		postContent.append("\n--" + WebHelper.HTTP_POST_BOUNDARY + "--\n");

		out.write(postContent.toString().getBytes());
		out.flush();
	}

	/**
	 * @param url
	 * @throws IOException
	 */
	protected abstract void setupGETConnectionFromURL(String url)
			throws IOException;

	/**
	 * @param url
	 * @throws IOException
	 */
	protected abstract void setupPOSTConnectionFromURL(String url)
			throws IOException;

}
