/**
    Indybay Publish Helper: Backend That Allows Publishing To Indybay.org And is Used By Several Different Frontends
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.publishhelper.media;

/**
 * @author Zogren
 * 
 */
public class MediaHelper {

	public static final int MEDIA_TYPE_AUDIO = 3;
	public static final int MEDIA_TYPE_DOCUMENT = 4;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	/**
	 * Returns media type given a Urib aed off resolver call to get mime type or
	 * based off file extension
	 * 
	 * @param uri
	 * @return mediaType
	 */
	public int getMediaTypeFromFileName(final String fileName) {
		if (fileName.endsWith(".jpg") || fileName.endsWith(".gif")
				|| fileName.endsWith(".png") || fileName.endsWith(".jpeg")
				|| fileName.endsWith(".tiff") || fileName.endsWith(".bmp")) {
			return MediaHelper.MEDIA_TYPE_IMAGE;
			// for now all non images are considered video
		} else {
			return MediaHelper.MEDIA_TYPE_VIDEO;
		}

	}

}
