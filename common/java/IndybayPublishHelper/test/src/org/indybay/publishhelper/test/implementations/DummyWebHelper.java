package org.indybay.publishhelper.test.implementations;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.indybay.publishhelper.persistence.FileHelper;
import org.indybay.publishhelper.persistence.WebHelper;

/**
 * Main class for web communications
 * 
 * @author Zogren for Indybay.org
 */
public  class DummyWebHelper extends WebHelper{


	private  HttpURLConnection connection =null;
		

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#cleanupAfterPOST()
	 */

	protected void cleanup() throws IOException {
		if (connection!=null)
			connection.disconnect();
	}

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#getFileHelper()
	 */

	protected FileHelper getFileHelper() {
		return new DummyFileHelper();
	}

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#getPOSTInputStream()
	 */

	protected InputStream getInputStream() throws IOException {
		return connection.getInputStream();
	}

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#getPOSTOutputStream()
	 */

	protected OutputStream getOutputStream() throws IOException {
		OutputStream out=connection.getOutputStream();
		final DataOutputStream os = new DataOutputStream(out);
		return os;
	}

	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#setupPOSTConnectionFromURL()
	 */

	protected void setupPOSTConnectionFromURL(String url) throws IOException {
		final URL urlObj = new URL(url);
		connection = (HttpURLConnection) urlObj
				.openConnection();
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type",
				"multipart/form-data;  boundary="
						+ WebHelper.HTTP_POST_BOUNDARY);
		
	}
	
	/* (non-Javadoc)
	 * @see org.indybay.publishhelper.persistence.WebHelper#setupPOSTConnectionFromURL()
	 */

	protected void setupGETConnectionFromURL(String url) throws IOException {
		final URL urlObj = new URL(url);
		connection = (HttpURLConnection) urlObj
				.openConnection();
		connection.setUseCaches(false);

	}


}
