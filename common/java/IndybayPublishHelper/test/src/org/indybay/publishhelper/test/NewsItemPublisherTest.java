package org.indybay.publishhelper.test;

import java.util.Vector;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.indybay.publishhelper.controller.NewsItemPublisher;
import org.indybay.publishhelper.model.IndyPreferences;
import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.model.NewsItemAttachment;
import org.indybay.publishhelper.test.implementations.DummyWebHelper;

/**
 * @author  Zogren for Indybay.org
 *
 */
public class NewsItemPublisherTest extends TestCase {
	/**
	 * 
	 */
	public void testPostWithFiles() {
		try {
			IndyPreferences preferences= new IndyPreferences();
			//preferences.setBaseURL("www.indybay.org");
			
			final NewsItemPublisher publisher = new NewsItemPublisher(preferences, new DummyWebHelper());
			final NewsItem newNewsItem = new NewsItem(preferences);
			newNewsItem.setTitle("12345678");
			newNewsItem.setAuthor("Test author 222");
			newNewsItem.setEmail("Test email a@a.com.333");
			newNewsItem.setSummary("12345678");
			newNewsItem.setText("Test text 555555555555555555555555555");
			newNewsItem.setRelatedLink("Test text 6666");
			newNewsItem.setRegionId(0);
			newNewsItem.setTopicId(0);
			newNewsItem.setDisplayEmail(true);

			NewsItemAttachment newsItemAttachment = new NewsItemAttachment();
			String fileName = "/Users/zogen/desktop/iphone images/indybay_publisher.png";
			newsItemAttachment.setFilePath(fileName);
			newNewsItem.getAttachments().add(newsItemAttachment);

			Vector validationProblems=publisher.publishNewsItem(newNewsItem);
			if (validationProblems!=null){
				for (int i=0;i<validationProblems.size();i++){
					String valProb=(String)validationProblems.get(i);
					System.out.println(valProb);
				}
				fail("Validation Failed");
			}

		} catch (final Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * 
	 *//*
	public void testPostWithNoFile() {
		try {
			IndyPreferences preferences= new IndyPreferences();
			final NewsItemPublisher publisher = new NewsItemPublisher();
			final NewsItem newNewsItem = new NewsItem(preferences);
			newNewsItem.setTitle("Test title 111");
			newNewsItem.setAuthor("Test author 222");
			newNewsItem.setEmail("Test email a@a.com.333");
			newNewsItem.setSummary("Test summary 444");
			newNewsItem.setText("Test text 555555555555555555555555555");
			newNewsItem.setRelatedLink("Test text 6666");
			newNewsItem.setRegionId(0);
			newNewsItem.setTopicId(0);
			newNewsItem.setDisplayEmail(true);
			publisher.publishNewsItem(newNewsItem);

		} catch (final Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}*/
}
