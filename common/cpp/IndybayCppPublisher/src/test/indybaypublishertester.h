/*
 * IndybayPublisherTester.h
 *
 *  Created on: Mar 11, 2010
 *      Author: zogen
 */


#ifndef INDYBAYPUBLISHERTESTER_H_
#define INDYBAYPUBLISHERTESTER_H_

#include "../model/newsitem.h"

class IndybayPublisherTester
{
public:
  IndybayPublisherTester();
  virtual
  ~IndybayPublisherTester();

  void runGETTest();

  void runPOSTTest(bool includeFiles);

  void runNewsItemPublishTest(bool includeFiles, bool hasValidatorErrors);

  void runTests();

  NewsItem getTestNewsItem(bool withAttachments, bool hasValidatorErrors) ;

};

#endif /* INDYBAYPUBLISHERTESTER_H_ */
