/*
 IndyPreferences class for holding default preferences for NewsItem
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INDYPREFERENCES_H
#define INDYPREFERENCES_H

#include <string>
using namespace std;

#include "indyserializablemodel.h"


class IndyPreferences  :  public IndySerializableModel
{
private:
	int resizeAmount;
    string applicationKey;
    string baseURL;
    string defaultAuthor;
    string defaultSummary;
    string defaultText ;
    string defaultTitle;
    string httpProtocol;
    int maxFilesAllowed;
    string personalKey;
    string relativePublishPageURLPath ;
    bool resetAuthor;
    bool resetSummary ;
    bool resetText ;
    bool resetTitle;
public:

	IndyPreferences();
	void setValueBasedOffKey(string key, string value);
    string toString();
    string getApplicationKey() const;
    string getBaseURL() const;
    string getDefaultAuthor() const;
    string getDefaultSummary() const;
    string getDefaultText() const;
    string getDefaultTitle() const;
    string getHttpProtocol() const;
    int getMaxFilesAllowed();
    string getPersonalKey() const;
    string getRelativePublishPageURLPath() const;
    bool getResetAuthor();
    bool getResetSummary();
    bool getResetText();
    bool getResetTitle();
    int getResizeAmount();

    void setBaseURL(string baseURL);
    void setDefaultAuthor(string defaultAuthor);
    void setDefaultSummary(string defaultSummary);
    void setDefaultText(string defaultText);
    void setDefaultTitle(string defaultTitle);
    void setMaxFilesAllowed(int maxFilesAllowed);
    void setPersonalKey(string personalKey);
    void setRelativePublishPageURLPath(string relativePublishPageURLPath);
    void setResetAuthor(bool resetAuthor);
    void setResetSummary(bool resetSummary);
    void setResetText(bool resetText);
    void setResetTitle(bool resetTitle);
    void setResizeAmount(int resizeAmount);

};

#endif // INDYPREFERENCES_H
