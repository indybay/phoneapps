/*
 optionList is a model class used for holding lists of topics or regions
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#import "optionList.h"
#import "optionListitem.h"

//returns an option name given an option id
string OptionList::getOptionNameFromId(int id){
	for (int i=0;i<optionList.size();i++){
		if (optionList.at(i).id==id){
			return optionList.at(i).name;
		}
	}
	return optionList.at(0).name;
}

//returns row number from an option list given an id
int OptionList::getRowNumFromId(int id){
	for (int i=0;i<optionList.size();i++){		
		if (optionList.at(i).id==id){
			return i;
		}
	}
	return 0;
}

//sets up option list with topics
OptionList* OptionList::newTopicList(){
	OptionList* newOptionList= new OptionList();
	newOptionList->optionList.push_back(* new OptionListItem(0, "No Topic Selected"));
	newOptionList->optionList.push_back(* new OptionListItem(18, "Anti-war"));
	newOptionList->optionList.push_back(* new OptionListItem(34, "Arts"));
	newOptionList->optionList.push_back(* new OptionListItem(27, "Drug War"));
	newOptionList->optionList.push_back(* new OptionListItem(30, "Education"));
	newOptionList->optionList.push_back(* new OptionListItem(33, "En Español"));
	newOptionList->optionList.push_back(* new OptionListItem(14, "Environment" ));;
	newOptionList->optionList.push_back(* new OptionListItem(22, "Anti-Capitalism" ));
	newOptionList->optionList.push_back(* new OptionListItem(45, "Government"));
	newOptionList->optionList.push_back(* new OptionListItem(56, "Immigration" ));
	newOptionList->optionList.push_back(* new OptionListItem(19, "Labor" ));
	newOptionList->optionList.push_back(* new OptionListItem(29, "LGBT / Queer" ));
	newOptionList->optionList.push_back(* new OptionListItem(32, "Media"));
	newOptionList->optionList.push_back(* new OptionListItem(13, "Police State"));
	newOptionList->optionList.push_back(* new OptionListItem(15, "Racial Justice"));
	newOptionList->optionList.push_back(* new OptionListItem(31, "Womyn"));
	newOptionList->optionList.push_back(* new OptionListItem(51, "Afghanistan"));
	newOptionList->optionList.push_back(* new OptionListItem(53, "Americas"));
	newOptionList->optionList.push_back(* new OptionListItem(50, "Haiti"));
	newOptionList->optionList.push_back(* new OptionListItem(48, "Iraq"));
	newOptionList->optionList.push_back(* new OptionListItem(29, "Palestine"));
	return newOptionList;
}

//sets up option list with regions
OptionList* OptionList::newRegionList(){
	OptionList* newOptionList= new OptionList();
	newOptionList->optionList.push_back(* new OptionListItem(0, "No Region Select"));
	newOptionList->optionList.push_back(* new OptionListItem(42, "California"));
	newOptionList->optionList.push_back(* new OptionListItem(35, "Central Valley / Sacramento"));
	newOptionList->optionList.push_back(* new OptionListItem(36, "San Francisco"));
	newOptionList->optionList.push_back(* new OptionListItem(38, "East Bay" ));
	newOptionList->optionList.push_back(* new OptionListItem(44, "International" ));
	newOptionList->optionList.push_back(* new OptionListItem(40, "North Bay / Marin"));
	newOptionList->optionList.push_back(* new OptionListItem(41, "North Coast"));
	newOptionList->optionList.push_back(* new OptionListItem(39, "Peninsula" ));
	newOptionList->optionList.push_back(* new OptionListItem(61, "San Diego" ));
	newOptionList->optionList.push_back(* new OptionListItem(60, "Santa Cruz"));
	newOptionList->optionList.push_back(* new OptionListItem(37, "South Bay / San Jose"));
	newOptionList->optionList.push_back(* new OptionListItem(43, "U.S."));
	return newOptionList;
	
}

