/*
 NewsItem class for holding default data for posting to Indybay.org
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef NEWSITEM_H
#define NEWSITEM_H

#include <string>
#include <vector>
using namespace std;

#include "indyserializablemodel.h"
#include "indypreferences.h"
#include "newsitemattachment.h"

class NewsItem: public IndySerializableModel {

private:
	string author;
	bool displayEmail;
	string email;
	int id;
	string postedURL;
	time_t publishDate;
	int regionId;
	string relatedLink;
	string summary;
	string text;
	string title;
	int topicId;
	vector<NewsItemAttachment> attachments;

public:
	NewsItem(IndyPreferences* preferences);

	vector<NewsItemAttachment> getAttachments() const;
	string getAuthor() const;
	bool getDisplayEmail() const;
	string getEmail() const;
	int getId() const;
	string getPostedURL() const;
	time_t getPublishDate() const;
	int getRegionId() const;
	string getRelatedLink() const;
	string getSummary() const;
	string getText() const;
	string getTitle() const;
	int getTopicId() const;

	void setAuthor(string author);
	void setDisplayEmail(bool displayEmail);
	void setEmail(string email);
	void setId(int id);
	void setPostedURL(string postedURL);
	void setPublishDate(time_t publishDate);
	void setRegionId(int regionId);
	void setRelatedLink(string relatedLink);
	void setSummary(string summary);
	void setText(string text);
	void setTitle(string title);
	void setTopicId(int topicId);

	void addAttachment(NewsItemAttachment newItem);
	void removeAttachmentAtIndex(int index);
	void removeAllAttachments();
	void setThumbnailForAttachment(int index, void* data);
	void reorderAttachments(int index1, int index2);

	void reset(IndyPreferences* preferences);
	void setValueBasedOffKey(string key, string value);
	string toString();
	string getDelimitedAttachmentPaths();
	void loadAttachmentsFromPaths(string tempAttachmentString);
	void loadNewsItemFromPrefs(IndyPreferences* preferences);
	void removeUnreadableAttachments();

};

#endif // NEWSITEM_H
