/*
 NewsItem class for holding default data for posting to Indybay.org
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "newsitem.h"
#include "../common/stringhelper.h"
#include <iterator>

/**
 constructor requires preferences to allow initial values to be  NewsItem::set
 correctly
 */
NewsItem::NewsItem(IndyPreferences* preferences) {
	loadNewsItemFromPrefs(preferences);
}

void NewsItem::setValueBasedOffKey(string nextKey, string nextValue) {
	StringHelper stringHelper;
	if (nextKey == "title") {
		title = nextValue;
		return;
	}
	if (nextKey == "author") {
		author = nextValue;
		return;
	}
	if (nextKey == "email") {
		email = nextValue;
		return;
	}
	if (nextKey == "relatedlink") {
		relatedLink = nextValue;
		return;
	}
	if (nextKey == "summary") {
		summary = stringHelper.replaceAll(nextValue, '\t', '\n');
		return;
	}
	if (nextKey == "text") {
		text = stringHelper.replaceAll(nextValue, '\t', '\n');
		return;
	}
	if (nextKey == "topicid") {
		topicId = stringHelper.stringToInt(nextValue);
		return;
	}
	if (nextKey == "regionid") {
		regionId = stringHelper.stringToInt(nextValue);
		return;
	}
	if (nextKey == "displayemail") {
		int displayEmailAsInt = stringHelper.stringToInt(nextValue);
		if (displayEmailAsInt == 0) {
			displayEmail = false;
		} else {
			displayEmail = true;
		}
		return;
	}

	if (nextKey == "attachments") {

		this->loadAttachmentsFromPaths(nextValue);

		return;
	}
}

//Reset fields and erase attachments based off preferences
void NewsItem::reset(IndyPreferences* preferences) {
	if ( preferences->getResetAuthor()) {
		author = preferences->getDefaultAuthor();
	}
	if (preferences->getResetTitle()) {
		title = preferences->getDefaultTitle();
	}
	if (preferences->getResetSummary()) {
		summary = preferences->getDefaultSummary();
	}
	if (preferences->getResetText()) {
		text = preferences->getDefaultText();
	}

	//email = preferences.getDefaultEmail();
	//relatedLink = preferences.getDefaultRelatedLink();

	attachments.clear();
	dirty=true;
	
}

string NewsItem::toString() {
	StringHelper strhelper;
	string buffer = "";
	buffer.append("title:");
	buffer.append(title);
	buffer.append("\n");
	buffer.append("author:");
	buffer.append(author);
	buffer.append("\n");
	buffer.append("email:");
	buffer.append(email);
	buffer.append("\n");
	buffer.append("relatedlink:");
	buffer.append(relatedLink);
	buffer.append("\n");
	buffer.append("topicid:");
	buffer.append(strhelper.intToString(topicId));
	buffer.append("\n");
	buffer.append("regionid:");
	buffer.append(strhelper.intToString(regionId));
	buffer.append("\n");
	buffer.append("displayemail:");
	if (displayEmail) {
		buffer.append("1");
	} else {
		buffer.append("0");
	}
	buffer.append("\n");
	buffer.append("summary:");
	buffer.append(strhelper.replaceAll(summary, '\n', '\t'));
	buffer.append("\n");
	buffer.append("attachments:");
	buffer.append(getDelimitedAttachmentPaths());
	buffer.append("\n");
	buffer.append("text:");
	buffer.append(strhelper.replaceAll(text, '\n', '\t'));

	return buffer;
}

string NewsItem::getDelimitedAttachmentPaths() {
	string buffer = "";
	NewsItemAttachment nextAttachment;
	for (unsigned int i = 0; i < attachments.size(); i++) {
		nextAttachment = (NewsItemAttachment) attachments .at(i);
		buffer.append(nextAttachment.filePath);
		buffer.append(";");
	}
	return buffer;
}

void NewsItem::loadAttachmentsFromPaths(string tempAttachmentString) {

	attachments.clear();

	if (tempAttachmentString == "")
		return;

	int i = -1;
	int j = 0;
	NewsItemAttachment nextAttachment;
	string nextPath = "";

	while (j > -1) {
		j = tempAttachmentString.find(";", i + 1);
		if (j > -1) {
			nextPath = tempAttachmentString.substr(i + 1, j - (i + 1));
			nextAttachment = (*new NewsItemAttachment());
			nextAttachment.filePath = nextPath;
			nextAttachment.thumbnail = NULL;

			attachments.push_back(nextAttachment);
		}
		i = j;
	}
	this->removeUnreadableAttachments();
}

void NewsItem::removeUnreadableAttachments() {
	NewsItemAttachment* nextAttachment;
	for (int i = attachments.size() - 1; i > -1; i--) {
		nextAttachment = &attachments.at(i);
		FILE* pFile = fopen(nextAttachment->filePath.c_str(), "r");

		//only add if file exists and is readable
		if (pFile == NULL) {
			attachments.erase(attachments.begin() + i);
		} else {
			fclose(pFile);
		}
	}
}

void NewsItem::loadNewsItemFromPrefs(IndyPreferences* preferences) {
	author = preferences->getDefaultAuthor();
	title = preferences->getDefaultTitle();
	summary = preferences->getDefaultSummary();
	text = preferences->getDefaultText();
	email = "";
	relatedLink = "";
	topicId=0;
	regionId=0;
}

vector<NewsItemAttachment> NewsItem::getAttachments() const {
	return attachments;
}

string NewsItem::getAuthor() const {
	return author;
}

bool NewsItem::getDisplayEmail() const {
	return displayEmail;
}

string NewsItem::getEmail() const {
	return email;
}

int NewsItem::getId() const {
	return id;
}

string NewsItem::getPostedURL() const {
	return postedURL;
}

time_t NewsItem::getPublishDate() const {
	return publishDate;
}

int NewsItem::getRegionId() const {
	return regionId;
}

string NewsItem::getRelatedLink() const {
	return relatedLink;
}

string NewsItem::getSummary() const {
	return summary;
}

string NewsItem::getText() const {
	return text;
}

string NewsItem::getTitle() const {
	return title;
}

int NewsItem::getTopicId() const {
	return topicId;
}

void NewsItem::setAuthor(string author) {
	this->author = author;
	dirty=true;
}

void NewsItem::setDisplayEmail(bool displayEmail) {
	this->displayEmail = displayEmail;
	dirty=true;
}

void NewsItem::setEmail(string email) {
	this->email = email;
	dirty=true;
}

void NewsItem::setId(int id) {
	this->id = id;
}

void NewsItem::setPostedURL(string postedURL) {
	if (this->postedURL!=postedURL){
		this->postedURL = postedURL;
		dirty=true;
	}
}

void NewsItem::setPublishDate(time_t publishDate) {
	this->publishDate = publishDate;
}

void NewsItem::setRegionId(int regionId) {
	if (this->regionId!=regionId){
		this->regionId = regionId;
		dirty=true;
	}
}

void NewsItem::setRelatedLink(string relatedLink) {
	if (this->relatedLink!=relatedLink){
		this->relatedLink = relatedLink;
		dirty=true;
	}
}

void NewsItem::setSummary(string summary) {
	if (this->summary != summary){
		this->summary = summary;
		dirty=true;
	}
}

void NewsItem::setText(string text) {
	if (this->text != text){
		this->text = text;
		dirty=true;
	}
}

void NewsItem::setTitle(string title) {
	if (this->title != title){
	this->title = title;
	dirty=true;
	}
}

void NewsItem::setTopicId(int topicId) {
	if (this->topicId != topicId){
		this->topicId = topicId;
		dirty=true;
	}
}


void  NewsItem::addAttachment(NewsItemAttachment newItem){
	attachments.push_back(newItem);
	dirty=true;
}

void  NewsItem::removeAttachmentAtIndex(int index){
	attachments.erase(attachments.begin()+index);
	dirty=true;
}

void  NewsItem::reorderAttachments(int to, int from){
	if (to != from) {
        NewsItemAttachment obj = attachments.at(from);
		attachments.erase(attachments.begin()+from);
        int attachSize=attachments.size();
		if (to >=attachSize ) {
			attachments.push_back(obj);
        } else {
			attachments.insert(attachments.begin()+to,obj);
        }
		dirty=true;
	}	
}

void NewsItem::removeAllAttachments(){
	if (attachments.size()>0){
		attachments.clear();
		dirty=true;
	}
}

void NewsItem::setThumbnailForAttachment(int index, void* data){
	if (attachments.at(index).thumbnail==NULL)
		attachments.at(index).thumbnail=data;
}

