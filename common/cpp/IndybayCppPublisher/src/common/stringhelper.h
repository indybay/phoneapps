/*
 StringHelper class for providing common string related functionality needed by IndybayPublisher code
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STRINGHELPER_H
#define STRINGHELPER_H

#include <string>
using namespace std;

#include "indypublishbase.h"

class StringHelper : public IndyPublishBase
{
private:
	int hexDigitToInt(char digit);
public:
	string intToString(int i);
	string intToHexString(int i);
	int stringToInt(string str);
	string replaceAll(string str1, char char1, char char2);
	int hexStringToInt(string str);
	string* getKeyAndValueFromString(string str, int startLineIndex, int endLineIndex);
};

#endif // STRINGHELPER_H
