/*
 IndyPublishLogger class for providing logging to other classes within the project
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INDYPUBLISHLOGGER_H
#define INDYPUBLISHLOGGER_H

#include <string>
using namespace std;

enum LogLevel {LEVEL_DEBUG,LEVEL_INFO,LEVEL_WARNING,LEVEL_ERROR};

class IndyPublishLogger
{
	
private:
	void log(string str, LogLevel level);
public:
	static string lastErrorMessage;
	void logError(string str);
	void logWarning(string  str);
	void logInfo(string  str);
	void logDebug(string  str);
	
};

#endif // INDYPUBLISHLOGGER_H