/*
 IndyPublishLogger class for providing logging to other classes within the project
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "indypublishlogger.h"
#include "indypublishexception.h"

string  IndyPublishLogger::lastErrorMessage="";

void IndyPublishLogger::log(string message,  LogLevel level){
	time_t rawtime;
	struct tm * timeinfo;
	char timeBuf [150];
	
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	
	strftime (timeBuf,64,"20%y-%m-%d %H:%M:%S %Z",timeinfo);
	
	//should do equivalent of vsnprintf but it is not ANSII so so cant
	//char* buffer= new char[str.length()+5000];
	//vsprintf(buffer, str.c_str(), argptr);
	
	const char*  logLevelString;
	switch (level) {
		case LEVEL_DEBUG:
			logLevelString="DEBUG";
			break;
		case LEVEL_INFO:
			logLevelString="INFO";
			break;
		case LEVEL_WARNING:
			logLevelString="WARNING";
			break;
		case LEVEL_ERROR:
			logLevelString="ERROR";
			break;
	}
	printf("%s %s *** %s\n",timeBuf,logLevelString,message.c_str());
}


void  IndyPublishLogger::logError(string str){
	log(str,LEVEL_ERROR);
	lastErrorMessage=str;
}

void  IndyPublishLogger::logWarning(string  str){
	log(str,LEVEL_WARNING);
}

void  IndyPublishLogger::logInfo(string  str){
	log(str,LEVEL_INFO);
}

void  IndyPublishLogger::logDebug(string str){
	log(str,LEVEL_DEBUG);
}
