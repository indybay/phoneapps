/*
 HTTPSocketReader class for helping reade chunked data from an HTTP socket 
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HTTPSOCKETREADER_H
#define HTTPSOCKETREADER_H

#include <map>
#include <vector>
#include <string>

#import <fstream>

using namespace std;

#import "sockethelper.h"


class HTTPSocketReader : public SocketHelper {
private:
	string readUpToSetLengthIntoString(int sockfd, int contentLength);
	int getNextChunkSizeFromStream(int sockfd);
	string getHeaderFromSocket(int sockfd);
public:
	bool receiveFromSocket(int sockfd, void* buffer, int expectedSize);
	HTTPSocketReader();
	string getFromSocket(int sockfd);
	
	
};


#endif // HTTPSOCKETREADER_H
