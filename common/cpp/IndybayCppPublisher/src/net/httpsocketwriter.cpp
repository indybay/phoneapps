/*
HTTPSocketWriter class for helping send chunked data over HTTP 
Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "httpsocketwriter.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../common/stringhelper.h"
#include "webhelper.h"
#include <math.h>

//eventually do in separate thread with timeout
int HTTPSocketWriter::sendBytesOverSocketHelper(int sockfd, const void* bytes, int amountToSend){
	//logger.logDebug("Entering sendBytesOverSocketHelper");
	int ret=send(sockfd,  bytes, amountToSend,0);
	WebHelper::bytesSentSoFarForCurrentPost+=ret;
	//logger.logDebug("Exiting sendBytesOverSocketHelper");
	return ret;
}

//send bytes over socket with optional chunk size string
bool HTTPSocketWriter::sendBytesOverSocket(int sockfd, const void* bytes, int amountToSendBits, bool sendChunkSizeFirst){
	//logger.logDebug("Entering sendBytesOverSocket");
	int amountSent=0;
	if (sendChunkSizeFirst){
		sendChunkSize(sockfd, amountToSendBits);
	}
	while (amountSent<amountToSendBits){
		amountSent=sendBytesOverSocketHelper(sockfd, bytes, amountToSendBits);
		if (amountSent==-1)
			return false;
		bytes=(void*)((long)bytes+amountSent);
		amountToSendBits-=amountSent;
	}
	return true;
	//logger.logDebug("Exiting sendBytesOverSocket");
}

//sends string over socket with optional chunk size string
bool HTTPSocketWriter::sendStringOverSocket(int sockfd, string str, bool sendChunkSizeFirst){
	//logger.logDebug("Entering sendStringOverSocket");
	const char* bytes=str.c_str();
	int sizeInBits=str.length();
	return sendBytesOverSocket(sockfd, (const void*)bytes, sizeInBits,sendChunkSizeFirst);
	//logger.logDebug("Exiting sendStringOverSocket");

}


//sends chunk size over socket
bool  HTTPSocketWriter::sendChunkSize(int sockfd, int chunkSizeBits){
	StringHelper stringHelper;
	string chunkSizeStr=clrf+stringHelper.intToHexString(chunkSizeBits)+clrf;
	return sendStringOverSocket(sockfd,chunkSizeStr,false);
}

//stream a file through a socket with chunk sizes optional before each write
bool HTTPSocketWriter::copyFileDataToSocket(int sockfd, const char* filePath, bool addChunkSizes){
	//logger.logDebug("Entering copyFileDataToSocket");
    FILE* fp;
    void* data;
    int numberOfBytesActuallyRead=0;
	string chunkSizeStr;
	bool success=true;
    if ((fp=fopen(filePath,"r"))==NULL){
		string errorMessage;
		errorMessage.append("Cannot open file ");
		errorMessage+=filePath;
		logger.logError(errorMessage);
		return false;
    }else{
		int amountToReadPerTry=pow(2,16);
        data=calloc(1,amountToReadPerTry);
		while (!feof(fp)){
			
            numberOfBytesActuallyRead=fread(data,1,amountToReadPerTry,fp);
			if (numberOfBytesActuallyRead!=0){
				if (!sendBytesOverSocket(sockfd, data, numberOfBytesActuallyRead, addChunkSizes)){
					success=false;
					break;
				}
			}
			//printf("clock:%d, amountOfFileReadTotal=%d, timeDiff=%d, currentBPS=%4.2f, eptimeleft=%4.2f\n",clock(),amountOfFileReadTotal,timeDiff,currentBPS,WebHelper::estimatedPostTimeLeft);
        }
				
		fclose(fp);
        free(data);
		return success;
		//logger.logDebug("Exiting copyFileDataToSocket");
		
    }
}
