/*
 SocketHelper class which is parent class of socket readig and writing classes
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SOCKETHELPER_H
#define SOCKETHELPER_H

#include <map>
#include <vector>
#include <string>
#import <fstream>
using namespace std;

#import "../common/iohelper.h"

class SocketHelper : public IOHelper {
public:
	SocketHelper();
	int setupSocketAndConnect(string baseURL);
};




#endif // SOCKETHELPER_H
