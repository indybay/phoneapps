/*
 HTTPSocketReader class for helping read chunked data from an HTTP socket 
 Copyright (C) 2010  Zachary Ogren / San Francisco Bay Area Indepdent Media Center
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#import "httpsocketreader.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../common/stringhelper.h"
#include <math.h>
#include "../common/indypublishexception.h"
#include <pthread.h>
#include <unistd.h>
HTTPSocketReader::HTTPSocketReader(){
	
}


bool HTTPSocketReader::receiveFromSocket(int sockfd, void* buffer, int expectedSize){
	if (expectedSize==0){
		return true;
	}
	int amountReceived=0;
	
	while (amountReceived<expectedSize){
		amountReceived=recv(sockfd, buffer, expectedSize, 0);
		if (amountReceived==0){
			logger.logError("Did Not Get Expected Amount From Socket");
			return false;
		}
		buffer=(void *)((long)buffer+amountReceived);
	}
	return true;
}

int HTTPSocketReader::getNextChunkSizeFromStream(int sockfd ){
	logger.logDebug("Entering getNextChunkSizeFromStream");
	StringHelper stringHelper;
	char* nextChar= new char[1];
	int  byte_count=0;
	bool hitReturn=false;
	bool hitNonReturn=false;
	string nextHex="";
	
	while (!hitReturn){
		receiveFromSocket(sockfd, nextChar, 1);
		if (*nextChar=='\n' && hitNonReturn){
			hitReturn=true;
		}else if (*nextChar!='\r' && *nextChar!='\n' ){
			hitNonReturn=true;
			nextHex+=nextChar;
		}
		if (nextHex.size()>8){
			logger.logError("Error getting chunk size from '"+nextHex+"'");
			return -1;
		}
	}
	
	if (nextHex.size()>0){
		
		byte_count=stringHelper.hexStringToInt(nextHex);
	}
	logger.logDebug("Exiting getNextChunkSizeFromStream");
	return byte_count;
	
}


string HTTPSocketReader::readUpToSetLengthIntoString(int sockfd, int contentLength){
	void * returnBuffer=(void*)new char[contentLength];
	string newStr;
	if (!receiveFromSocket(sockfd, returnBuffer,contentLength))
		return "";
	newStr.append((char*)returnBuffer,contentLength);
	
	return newStr;
}


string HTTPSocketReader::getFromSocket(int sockfd){
	//get response header
	StringHelper stringHelper;
	
    string header=getHeaderFromSocket(sockfd);
	
	//make sure response as HTTP
	int isOkLoc=header.find("200");
	string newStr;
	int contentLength=0;
	if (isOkLoc<0 || isOkLoc>20){
		logger.logWarning("Header was not ok but continuing anyways in case there is debug info\n"+header);
	}
		char contentLengthKey[]="Content-Length:";
		int j=header.find(contentLengthKey);
		if (j>0){
			int jj=header.find("\n",j);
			j=j+sizeof(contentLengthKey);
			
			string contentLengthStr=header.substr(j, jj-j);
			contentLength=stringHelper.stringToInt(contentLengthStr);
			newStr.append(readUpToSetLengthIntoString(sockfd,contentLength));
			if (IndyPublishLogger::lastErrorMessage!=""){
				return "";
			}
			
		}else{
			j=header.find("Transfer-Encoding: chunked");
			if (j>0){
				int nextByteCount;
				while ((nextByteCount=getNextChunkSizeFromStream(sockfd)) > 0){
					newStr.append(readUpToSetLengthIntoString(sockfd,nextByteCount));
					if (IndyPublishLogger::lastErrorMessage!=""){
						return "";
					}
				}
			}else{
				logger.logError("No content length or chunk encoding\n"+header);
				return "";
			}
		}
	return newStr;
}


//gets from an HTTP header from a socket stopping at first double return
string HTTPSocketReader::getHeaderFromSocket(int sockfd){
	int i=0;
	string header;
	bool doubleReturn=false;
	bool singleReturn=false;
	char* charBuf= new char[1];
	while (i<5000 && !doubleReturn){
		if (!receiveFromSocket(sockfd,charBuf,1))
			return "";
		header.append(charBuf);
		if (*charBuf=='\n'){
			if (singleReturn){
				doubleReturn=true;
			}else{
				singleReturn=true;
			}
		}else if (*charBuf!='\r'){
			singleReturn=false;
		}
	}
	//logger.logDebug("Header was \n---\n"+header+"\n---\n");
	return header;
}


