/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.viewcontroller;

import java.io.IOException;

import org.indybay.androidpublisher.media.AndroidMediaHelper;
import org.indybay.androidpublisher.persistence.AndroidFileHelper;
import org.indybay.androidpublisher.persistence.AndroidNewsItemPersister;
import org.indybay.androidpublisher.uielement.ImageGalleryAdapter;
import org.indybay.publishhelper.model.NewsItemAttachment;
import org.indybay.publishhelper.persistence.FileHelper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Controller for Attachments List Page Aside from displayig attachment contains
 * the code called when the add attachment menu item is called
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class PhotoListPageController extends AndroidPublisherController
		implements View.OnClickListener {

	public static boolean forceSelectImage = false;
	/**
	 * gallery displaying srolling version of thumbnails of selected images,
	 * videos etc...
	 * 
	 */
	private Gallery gallery = null;

	/**
	 * message displaying the number of items selected
	 */
	private TextView photoText = null;

	/**
	 * 
	 */
	private Button removeButton = null;

	/**
	 * currently selected photo index (-1 means none are selected)
	 */
	private int selectedPhotoNum = -1;

	/*
	 * (non-Javadoc)
	 * 
	 * This method is needed to respond to clicks on things. In this case this
	 * is the remove button for an image in the gallery
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(final View view) {

		if (view == this.removeButton) {
			if ((this.selectedPhotoNum != -1)
					&& (AndroidPublisherController.newsItem != null)
					&& (AndroidPublisherController.newsItem.getAttachments() != null)) {
				if (this.selectedPhotoNum < AndroidPublisherController.newsItem
						.getAttachments().size()) {
					AndroidPublisherController.newsItem.getAttachments()
							.remove(this.selectedPhotoNum);
					try {
						final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
								this.baseFilePathForNewsItem);
						persister.save(AndroidPublisherController.newsItem);

					} catch (final IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					this.selectedPhotoNum = -1;
				}
				((ImageGalleryAdapter) this.gallery.getAdapter())
						.notifyDataSetChanged();
			}
			this.refreshFormFromNewsItem();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * Sets up references to all of the UI components on the page and setup the
	 * custom class for the gallery
	 * 
	 * @see
	 * org.indybay.androidpublisher.viewcontroller.AndroidPublisherController
	 * #onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {

		this.selectedPhotoNum = -1;

		this.setContentView(R.layout.photo_list);
		super.onCreate(savedInstanceState);

		this.photoText = (TextView) this.findViewById(R.id.photonum_label);

		this.removeButton = (Button) this
				.findViewById(R.id.plist_remove_selected);
		this.removeButton.setOnClickListener(this);

		this.gallery = (Gallery) this.findViewById(R.id.plist_image_gallery);

		this.gallery.setAdapter(new ImageGalleryAdapter(this));

		this.gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(final AdapterView parent, final View v,
					final int position, final long id) {
				PhotoListPageController.this.selectedPhotoNum = position;

				PhotoListPageController.this.removeButton
						.setVisibility(View.VISIBLE);
				final NewsItemAttachment selectedAttachment = (NewsItemAttachment) AndroidPublisherController.newsItem
						.getAttachments().get(position);
				final FileHelper fileHelper = new AndroidFileHelper();
				Toast
						.makeText(
								PhotoListPageController.this.getBaseContext(),
								"Attachment #"
										+ (position + 1)
										+ "\n"
										+ fileHelper
												.getFilenameFromFilePath(selectedAttachment
														.getFilePath())
										+ "\nSelected", Toast.LENGTH_SHORT)
						.show();
			}
		});

		if (PhotoListPageController.forceSelectImage) {
			PhotoListPageController.forceSelectImage = false;
			this.selectFromDisk();
		}

		this.refreshFormFromNewsItem();
	}

	/**
	 * action when user chooses menu item to select a file from the disk
	 */
	private void selectFromDisk() {
		final Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("video/*,image/*");
		this.startActivityForResult(photoPickerIntent, 1);
	}

	/**
	 * given an intent resulting from the callback to onActivity result after a
	 * file was selected, sets up the Attachment, adds it to the over and resets
	 * the gallery
	 * 
	 * @param newAttachment
	 * @param photoUri
	 * @throws IOException
	 */
	private void setAttachmentDataForSelectedFile(
			final NewsItemAttachment newAttachment, final Intent intent) {
		try {
			final Uri photoUri = intent.getData();

			final String path = AndroidMediaHelper.getFilePathFromUri(photoUri,
					this.getContentResolver());
			newAttachment.setFilePath(path);

		} catch (final Exception e) {
			this.displayError("Error selecting file: " + intent.getData()
					+ ". Error was " + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * call back after a file was selected
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode == Activity.RESULT_OK) {
			final NewsItemAttachment newAttachment = new NewsItemAttachment();

			this.setAttachmentDataForSelectedFile(newAttachment, intent);

			AndroidPublisherController.newsItem.getAttachments().add(
					newAttachment);
			try {
				final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
						this.baseFilePathForNewsItem);
				persister.save(AndroidPublisherController.newsItem);
			} catch (final IOException e) {
				Log.e("PhotoListPageController", "onActivityResult", e);
			}
			((ImageGalleryAdapter) this.gallery.getAdapter())
					.notifyDataSetChanged();

			this.refreshFormFromNewsItem();

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.androidpublisher.viewcontroller.AndroidPublisherController
	 * #refreshFormFromNewsItem()
	 */
	@Override
	protected void refreshFormFromNewsItem() {

		String photoTextText = null;
		if (AndroidPublisherController.newsItem.getAttachments().size() == 0) {
			photoTextText = "No Attachments Selected";
		} else if (AndroidPublisherController.newsItem.getAttachments().size() == 1) {
			photoTextText = "1 Attachment Selected";
		} else {
			photoTextText = AndroidPublisherController.newsItem
					.getAttachments().size()
					+ " Attachments Selected";
		}
		this.photoText.setText(photoTextText);

		if ((AndroidPublisherController.newsItem.getAttachments() == null)
				|| (AndroidPublisherController.newsItem.getAttachments().size() == 0)
				|| (this.selectedPhotoNum == -1)) {
			this.removeButton.setVisibility(View.INVISIBLE);
		} else {
			this.removeButton.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Needed since it is abstract in parent but in this case there is nothing
	 * to save back to the newsitem (since the attachments themselves are added
	 * and removed directly)
	 */
	@Override
	protected void refreshNewsItemFromForm() {
		// nothing to save
	}

}
