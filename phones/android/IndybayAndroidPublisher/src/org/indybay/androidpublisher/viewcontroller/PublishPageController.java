/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.viewcontroller;

import java.io.IOException;
import java.util.Vector;

import org.indybay.androidpublisher.persistence.AndroidNewsItemPersister;
import org.indybay.androidpublisher.persistence.AndroidWebHelper;
import org.indybay.publishhelper.controller.NewsItemPublisher;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Controller class for the publish page This class is the first activity that
 * displays on opening the app and is also reesponsible for the calls to the
 * IndybayPublishHelper methods that do the actual validation and publishing
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class PublishPageController extends AndroidPublisherController implements
		View.OnClickListener {

	/**
	 * url of page that was just published. This is needed so the page can be
	 * reloaded once after publish so he reset is not undone by a back action
	 * from the web browser page showing the results
	 */
	private static String postedURL = null;

	/**
	 * hook to the input for the author name
	 */
	private EditText authorEditText = null;

	/**
	 * hook to the text that displays the number of attachments
	 */
	private TextView photoText = null;

	/**
	 * hook to the publish button
	 */
	private Button publishButton = null;

	/**
	 * hook to the summary input box
	 */
	private EditText summaryEditText = null;

	/**
	 * hook to the text input box
	 */
	private EditText textEditText = null;

	/**
	 * hook to the title input box
	 */
	private EditText titleEditText = null;

	/*
	 * (non-Javadoc)
	 * 
	 * This is the method called on all click actions. In this case the only
	 * action is publish
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(final View view) {
		if (view == this.publishButton) {
			this.publish();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * sets up all of the inputs on the page
	 * 
	 * @see
	 * org.indybay.androidpublisher.viewcontroller.AndroidPublisherController
	 * #onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		this.setContentView(R.layout.publish);

		super.onCreate(savedInstanceState);

		this.publishButton = (Button) this.findViewById(R.id.publish);
		this.publishButton.setOnClickListener(this);
		this.photoText = (TextView) this.findViewById(R.id.photonum_label);
		this.titleEditText = (EditText) this.findViewById(R.id.title_text);
		this.authorEditText = (EditText) this.findViewById(R.id.author_text);
		this.summaryEditText = (EditText) this.findViewById(R.id.summary_text);

		this.textEditText = (EditText) this.findViewById(R.id.text);

		this.refreshFormFromNewsItem();

		// this is needed for the reset after publishing to get to the web
		// browser with a back action
		// that includes the modified publish page
		if (PublishPageController.postedURL != null) {
			this.redirectToPage();
		}
	}

	/**
	 * This is needed for the reset after publishing to get to the web browser
	 * with a back action that includes the modified publish page
	 */
	public void redirectToPage() {
		final String lPostedURL = PublishPageController.postedURL;
		PublishPageController.postedURL = null;
		final Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri
				.parse(lPostedURL));
		this.startActivity(myIntent);
	}

	/**
	 * Displays vaoidation messages after publishing
	 * 
	 * @param validationProblems
	 */
	private void displayValidationMessage(final Vector validationProblems) {
		if (validationProblems == null) {
			return;
		}
		if (validationProblems.size() == 0) {
			this
					.displayError("displayValidationMessage called with no validation message");
			return;
		}
		final String firstProblem = (String) validationProblems.get(0);

		this.displayInfo(firstProblem);
	}

	/**
	 * publish action that POSTs the newsitem to the server
	 */
	private void publish() {

		try {
			this.refreshNewsItemFromForm();
			final NewsItemPublisher publisher = new NewsItemPublisher(
					this.preferences, new AndroidWebHelper());

			final Vector<String> validationProblems = publisher
					.publishNewsItem(AndroidPublisherController.newsItem);
			if (validationProblems != null) {
				if ((AndroidPublisherController.newsItem.getAttachments() == null)
						|| (AndroidPublisherController.newsItem
								.getAttachments().size() == 0)) {
					validationProblems
							.add("You Must Add At Least One Attachment");
				}
				this.displayValidationMessage(validationProblems);

			} else {

				// set the page we want to go to after a refresh to be the
				// result page in the pda format (so it doesnt display the
				// headers and left side bar)

				PublishPageController.postedURL = AndroidPublisherController.newsItem
						.getPostedURL()
						+ "?pda=true";
				AndroidPublisherController.newsItem.reset(this.preferences);
				final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
						this.getCacheDir().getAbsolutePath());
				persister.save(AndroidPublisherController.newsItem);
				this.refreshFormFromNewsItem();
				System.gc();

				final Intent myIntent = new Intent(this,
						PublishPageController.class);
				this.startActivity(myIntent);

			}
		} catch (final Exception e) {
			this.displayError("Uncaught Exception: " + e.toString() + ": "
					+ e.getMessage());
			Log.e("PublishPageController.publish", "Error: " + e.getMessage(),
					e);
		}
	}

	/**
	 * sets all the fields on this page from the newsitem
	 */
	@Override
	protected void refreshFormFromNewsItem() {
		this.titleEditText.setText(AndroidPublisherController.newsItem
				.getTitle());
		this.summaryEditText.setText(AndroidPublisherController.newsItem
				.getSummary());
		this.textEditText
				.setText(AndroidPublisherController.newsItem.getText());
		this.authorEditText.setText(AndroidPublisherController.newsItem
				.getAuthor());

		String photoTextText = null;
		if (AndroidPublisherController.newsItem.getAttachments().size() == 0) {
			photoTextText = "No Attachments Selected";
		} else if (AndroidPublisherController.newsItem.getAttachments().size() == 1) {
			photoTextText = "1 Attachment Selected";
		} else {
			photoTextText = AndroidPublisherController.newsItem
					.getAttachments().size()
					+ " Attachments Selected";
		}
		this.photoText.setText(photoTextText);
	}

	/**
	 * Updates and saves the newsitem from all of the inputs on the page
	 */
	@Override
	protected void refreshNewsItemFromForm() {
		AndroidPublisherController.newsItem.setTitle(this.titleEditText
				.getText().toString());
		AndroidPublisherController.newsItem.setSummary(this.summaryEditText
				.getText().toString());
		AndroidPublisherController.newsItem.setText(this.textEditText.getText()
				.toString());
		AndroidPublisherController.newsItem.setAuthor(this.authorEditText
				.getText().toString());
		try {
			final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
					this.baseFilePathForNewsItem);
			persister.save(AndroidPublisherController.newsItem);
		} catch (final IOException e) {
			Log.e("PublishPageController.refreshNewsItemFromForm", "Error: "
					+ e.getMessage(), e);
		}
	}

}