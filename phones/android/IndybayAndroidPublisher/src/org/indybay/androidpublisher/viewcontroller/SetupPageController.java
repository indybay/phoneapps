/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.viewcontroller;

import java.io.IOException;

import org.indybay.androidpublisher.persistence.AndroidNewsItemPersister;

import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Controller for the setup page tha mainly sets the preferences object used by
 * the publish page
 * 
 * @author Zogren for Indybay.org
 * 
 */
public class SetupPageController extends AndroidPublisherController {

	private EditText baseURLEditText = null;
	private EditText defaultAuthorText = null;
	private EditText defaultSummaryText = null;
	private EditText defaultTextText = null;
	private EditText defaultTitleText = null;
	private EditText emailText = null;
	private EditText maxUploadsText = null;
	private EditText relatedLinkText = null;
	private CheckBox resetAuthor = null;
	private CheckBox resetSummary = null;
	private CheckBox resetText = null;
	private CheckBox resetTitle = null;
	private EditText userKeyText = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.androidpublisher.viewcontroller.AndroidPublisherController
	 * #onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		this.setContentView(R.layout.setup);
		super.onCreate(savedInstanceState);

		this.baseURLEditText = (EditText) this.findViewById(R.id.base_url);
		this.userKeyText = (EditText) this.findViewById(R.id.personal_key);

		this.defaultTitleText = (EditText) this
				.findViewById(R.id.default_title);
		this.defaultAuthorText = (EditText) this
				.findViewById(R.id.default_author);
		this.defaultSummaryText = (EditText) this
				.findViewById(R.id.default_summary);
		this.defaultTextText = (EditText) this.findViewById(R.id.default_text);

		this.emailText = (EditText) this.findViewById(R.id.email);
		this.relatedLinkText = (EditText) this.findViewById(R.id.related_link);

		this.maxUploadsText = (EditText) this.findViewById(R.id.max_uploads);

		this.resetTitle = (CheckBox) this.findViewById(R.id.reset_title);
		this.resetAuthor = (CheckBox) this.findViewById(R.id.reset_author);
		this.resetSummary = (CheckBox) this.findViewById(R.id.reset_summary);
		this.resetText = (CheckBox) this.findViewById(R.id.reset_text);

		this.refreshFormFromNewsItem();
	}

	/**
	 * fill in all of the setup fields from the preferences object and newsitem
	 */
	@Override
	protected void refreshFormFromNewsItem() {

		this.baseURLEditText.setText(this.preferences.getBaseURL());
		this.userKeyText.setText(this.preferences.getPersonalKey());

		this.defaultTitleText.setText(this.preferences.getDefaultTitle());
		this.defaultAuthorText.setText(this.preferences.getDefaultAuthor());
		this.defaultSummaryText.setText(this.preferences.getDefaultSummary());
		this.defaultTextText.setText(this.preferences.getDefaultText());
		this.emailText.setText(this.preferences.getDefaultEmail());
		this.relatedLinkText.setText(this.preferences.getDefaultRelatedLink());

		this.resetTitle.setChecked(this.preferences.isResetTitle());
		this.resetAuthor.setChecked(this.preferences.isResetAuthor());
		this.resetSummary.setChecked(this.preferences.isResetSummary());
		this.resetText.setChecked(this.preferences.isResetText());

		this.maxUploadsText.setText(String.valueOf(this.preferences
				.getMaxFilesAllowed()));
	}

	/**
	 * save from the UI to the preferences file
	 */
	@Override
	protected void refreshNewsItemFromForm() {

		AndroidPublisherController.newsItem.setEmail(this.emailText.getText()
				.toString());
		AndroidPublisherController.newsItem.setRelatedLink(this.relatedLinkText
				.getText().toString());
		try {
			final AndroidNewsItemPersister persister = new AndroidNewsItemPersister(
					this.baseFilePathForNewsItem);
			persister.save(AndroidPublisherController.newsItem);
		} catch (final IOException e1) {
			Log.e("SetupPageController.refreshNewsItemFromForm", "Error: "
					+ e1.getMessage(), e1);
		}

		this.preferences.setBaseURL(this.baseURLEditText.getText().toString());
		this.preferences.setPersonalKey(this.userKeyText.getText().toString());

		this.preferences.setDefaultTitle(this.defaultTitleText.getText()
				.toString());
		this.preferences.setDefaultAuthor(this.defaultAuthorText.getText()
				.toString());
		this.preferences.setDefaultSummary(this.defaultSummaryText.getText()
				.toString());
		this.preferences.setDefaultText(this.defaultTextText.getText()
				.toString());
		this.preferences.setDefaultEmail(this.emailText.getText().toString());
		this.preferences.setDefaultRelatedLink(this.relatedLinkText.getText()
				.toString());

		this.preferences.setResetTitle(this.resetTitle.isChecked());
		this.preferences.setResetAuthor(this.resetAuthor.isChecked());
		this.preferences.setResetSummary(this.resetSummary.isChecked());
		this.preferences.setResetText(this.resetText.isChecked());

		try {
			this.preferences.setMaxFilesAllowed(Integer
					.parseInt(this.maxUploadsText.getText().toString()));
		} catch (final Exception e) {
			Log.e("SetupPageController.refreshNewsItemFromForm", "Error: "
					+ e.getMessage(), e);
		}
		this.preferences.save();
	}

}