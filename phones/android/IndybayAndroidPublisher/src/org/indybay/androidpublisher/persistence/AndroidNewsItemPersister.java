/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.indybay.publishhelper.model.NewsItem;
import org.indybay.publishhelper.persistence.NewsItemPersister;

import android.util.Log;

/**
 * @author Zogren
 * 
 */
public class AndroidNewsItemPersister extends NewsItemPersister {

	private String basePath = null;

	/**
	 * 
	 */
	private final String newsItemFilePath = "currentNewsItem.ips";

	public AndroidNewsItemPersister(String newBasePath) {
		if (!newBasePath.endsWith("/")) {
			newBasePath = newBasePath + "/";
		}
		this.basePath = newBasePath;
	}

	/**
	 * Sets up a news item by either loading it from a cached file or setting up
	 * a new one with default values
	 * 
	 * @param filePath
	 * @param preferences
	 * @return newsItem
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */

	@Override
	public NewsItem load()

	{

		NewsItem newsItem = null;
		try {
			final File fc = new File(this.basePath + this.newsItemFilePath);

			if (fc.exists()) {
				try {
					String str = "";
					final InputStream is = new FileInputStream(fc);
					final byte[] data = new byte[is.available()];
					is.read(data);
					is.close();
					final NewsItem newNewsItem = new NewsItem();
					str = new String(data);
					newNewsItem.loadFromString(str);
					newsItem = null;
					System.gc();
					newsItem = newNewsItem;

				} catch (final Exception ee) {
					Log.e("AndroidNewsItemPersister", "Error", ee);
					ee.printStackTrace();
					fc.delete();
				}
			}
		} catch (final Exception ee) {
			Log.e("AndroidNewsItemPersister", "Error", ee);
			ee.printStackTrace();

		}
		return newsItem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.java.persistence.NewsItemPersister#save(org.indybay.java.
	 * model.NewsItem)
	 */
	@Override
	public void save(final NewsItem newsItem) throws IOException {

		final File fc = new File(this.basePath + this.newsItemFilePath);
		if (fc.exists()) {
			fc.delete();
		}

		final OutputStream os = new FileOutputStream(fc);

		os.write(newsItem.toString().getBytes());
		os.close();

	}

}
