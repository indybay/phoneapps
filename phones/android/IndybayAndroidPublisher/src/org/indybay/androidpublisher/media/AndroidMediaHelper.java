/**
	Indybay Android Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.androidpublisher.media;

import java.io.IOException;

import org.indybay.androidpublisher.persistence.AndroidFileHelper;
import org.indybay.publishhelper.media.MediaHelper;
import org.indybay.publishhelper.persistence.FileHelper;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.MediaColumns;

/**
 * Android specific media related methods (many of which make calls off to the
 * VideoHelper or ImageHelper based off the determined type)
 * 
 * 
 * @author Zogren for Indybay.org
 * 
 */

public class AndroidMediaHelper extends MediaHelper {

	/**
	 * Returns a file name gieven a Uri
	 * 
	 * @param photoUri
	 * @param contentResolver
	 * @return filename
	 */
	public static String getFilenameFromUri(final Uri photoUri,
			final ContentResolver contentResolver) {

		final String filePath = AndroidMediaHelper.getFilePathFromUri(photoUri,
				contentResolver);
		final FileHelper fileHelper = new AndroidFileHelper();
		return fileHelper.getFilenameFromFilePath(filePath);
	}

	/**
	 * Returns a file path given a Uri
	 * 
	 * @param photoUri
	 * @param contentResolver
	 * @return filepath
	 */
	public static String getFilePathFromUri(final Uri photoUri,
			final ContentResolver contentResolver) {

		if (photoUri.toString().startsWith("file:")) {
			return photoUri.getPath();
		}
		final String[] projection = { MediaColumns.DATA, /* col1 */
		MediaColumns.DISPLAY_NAME
		/* col2 */};

		String fileName = null;
		final Cursor c = contentResolver.query(photoUri, projection, null,
				null, null);
		if ((c != null) && c.moveToFirst()) {
			fileName = c.getString(0);
		}
		return fileName;
	};

	/**
	 * activity is needed since it has references to the current file context
	 */
	private Activity context = null;

	/**
	 * needed for many image related activities
	 */
	private ContentResolver resolver = null;

	/**
	 * constructor reqires an Activity and sets both that and the context in
	 * local variables
	 * 
	 * @param newContext
	 */
	public AndroidMediaHelper(final Activity newContext) {
		this.context = newContext;
		this.resolver = this.context.getContentResolver();
	}

	/**
	 * Returns thumbnail data for a video or image based off parent Uri (either
	 * calls ImageHelper or VideoHelper)
	 * 
	 * @param uri
	 * @return thubnail data
	 * @throws IOException
	 */
	public byte[] getThumbnailDataGivenParentFilePath(final String filePath)
			throws IOException {
		final int type = this.getMediaTypeFromFileName(filePath);
		byte[] thumbnailData = null;
		if (type == MediaHelper.MEDIA_TYPE_IMAGE) {
			thumbnailData = ImageHelper.getThumbnailDataGivenParentFilePath(
					filePath, this.resolver);
		} else {
			thumbnailData = VideoHelper.getThumbnailDataGivenParentFilePath(
					filePath, this.context);
		}

		return thumbnailData;
	}

}
