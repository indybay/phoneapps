/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.viewcontroller;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.StringItem;

import org.indybay.j2me.controller.JavaMEFileHelper;
import org.indybay.j2me.media.JavaMEImageHelper;
import org.indybay.publishhelper.media.MediaHelper;
import org.indybay.publishhelper.model.NewsItemAttachment;

/**
 * @author Zogren for Indybay.org
 */
public class AttachmentListPage extends IndyPublisherPage implements
		CommandListener {

	private Vector checkBoxes = null;
	/**
     *
     */
	private final Command removeCommand = new Command("Remove Selected",
			Command.OK, 1);

	/**
     *
     */
	/**
	 * @param newParent
	 */
	public AttachmentListPage(final IndybayPublisherMIDlet newParent) {
		super(newParent, "Attachment List");
		this.addCommand(this.removeCommand);
		this.addCommonCommands(this.listAttachmentsCommand);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#commandAction(javax
	 * .microedition.lcdui.Command, javax.microedition.lcdui.Displayable)
	 */
	public void commandAction(final Command c, final Displayable d) {

		super.commandAction(c, d);

		if (c == this.removeCommand) {
			if (this.checkBoxes != null) {
				ChoiceGroup nextCheckBox = null;
				if (this.checkBoxes.size() != this.parentMidlet.newsItem
						.getAttachments().size()) {
					System.out
							.println("Error: list size different between checkboxes and attachments");
				} else {
					for (int i = this.checkBoxes.size() - 1; i > -1; i--) {
						nextCheckBox = (ChoiceGroup) this.checkBoxes
								.elementAt(i);
						if (nextCheckBox.isSelected(0)) {
							this.parentMidlet.newsItem.getAttachments()
									.removeElementAt(i);
						}
					}
				}
			}
			try {
				this.parentMidlet.saveNewsItem();
			} catch (final Exception e) {
				e.printStackTrace();
			}
			this.refreshForm();
		}

	}

	/**
     *
     */
	protected void refreshForm() {

		this.deleteAll();

		if (this.parentMidlet.newsItem.getAttachments().size() == 0) {
			final StringItem attachmentsField = new StringItem("",
					"No Attachments Selected");
			this.append(attachmentsField);
		} else {
			this.checkBoxes = new Vector();
			final JavaMEImageHelper imageHelper = new JavaMEImageHelper();
			final Enumeration attachements = this.parentMidlet.newsItem
					.getAttachments().elements();
			int i = 1;
			String nextStringPart = null;
			NewsItemAttachment nextAttachment = null;
			final JavaMEFileHelper fileHelper = new JavaMEFileHelper();
			int nextType = 0;
			Image nextImagePart = null;

			ChoiceGroup nextCheckBox = null;

			while (attachements.hasMoreElements()) {
				nextAttachment = (NewsItemAttachment) attachements
						.nextElement();
				nextStringPart = i
						+ ": "
						+ fileHelper.getFilenameFromFilePath(nextAttachment
								.getFilePath()) + "\n";
				nextImagePart = null;

				nextType = imageHelper.getMediaTypeFromFileName(nextAttachment
						.getFilePath());
				if (nextType == MediaHelper.MEDIA_TYPE_IMAGE) {
					try {
						// System.out.println("Image path " +
						// nextAttachment.getFilePath());

						final FileConnection fc = (FileConnection) Connector
								.open(nextAttachment.getFilePath());
						if (fc.exists()) {
							final InputStream is = fc.openInputStream();

							nextImagePart = Image.createImage(is);

							final int originalHeight = nextImagePart.getWidth();
							int height = originalHeight;
							if (height > this.getHeight() / 2) {
								height = this.getHeight() / 2;
							}

							nextImagePart = imageHelper.scaleImage(
									nextImagePart, (double) height
											/ (double) originalHeight);

							is.close();
							this.append(nextImagePart);

						}
					} catch (final Exception e) {
						e.printStackTrace();
						nextImagePart = null;
					}
				}
				nextCheckBox = new ChoiceGroup(null, Choice.MULTIPLE);
				nextCheckBox.append(nextStringPart, null);
				this.append(nextCheckBox);
				this.checkBoxes.addElement(nextCheckBox);
				i++;
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#refreshNewsItemFromForm
	 * ()
	 */
	protected void refreshNewsItem() {
	}
}
