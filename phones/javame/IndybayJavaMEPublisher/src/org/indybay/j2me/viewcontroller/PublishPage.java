/**
    Indybay Java ME Publisher: UI Front End For Publishing To Indybay.org From A Mobile Device
	
    Copyright (C) 2010 The San Francisco Bay Area Independent Media Center

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.indybay.j2me.viewcontroller;

import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;

import org.indybay.j2me.controller.JavaMEFileHelper;
import org.indybay.j2me.controller.JavaMEWebHelper;
import org.indybay.publishhelper.controller.NewsItemPublisher;
import org.indybay.publishhelper.model.NewsItemAttachment;

/**
 * @author Zogren for Indybay.org
 */
public class PublishPage extends IndyPublisherPage implements CommandListener {

	/**
     *
     */
	private StringItem attachmentsField = null;
	/**
     *
     */
	private TextField authorField = null;
	/**
     *
     */
	private final Command publishCommand = new Command("Publish", Command.OK, 1);
	/**
     *
     */
	private TextField summaryField = null;
	/**
     *
     */
	private TextField textField = null;
	/**
     *
     */
	private TextField titleField = null;

	/**
	 * @param newParent
	 */
	public PublishPage(final IndybayPublisherMIDlet newParent) {

		super(newParent, "Publish");

		this.summaryField = new TextField("Summary", this.parentMidlet.newsItem
				.getSummary(), 100, TextField.ANY);

		this.titleField = new TextField("Title", "", 30, TextField.ANY);

		this.authorField = new TextField("Author", "", 15, TextField.ANY);

		this.textField = new TextField("Text", "", 100, TextField.ANY);

		this.attachmentsField = new StringItem("", "");

		this.append(this.titleField);
		this.append(this.authorField);
		this.append(this.summaryField);
		this.append(this.textField);
		this.append(this.attachmentsField);

		this.addCommand(this.publishCommand);
		this.addCommonCommands(this.publishPageCommand);
		this.refreshForm();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#commandAction(javax
	 * .microedition.lcdui.Command, javax.microedition.lcdui.Displayable)
	 */
	public void commandAction(final Command c, final Displayable d) {

		super.commandAction(c, d);

		if (c == this.publishCommand) {
			this.publish();
		}

	}

	/**
     *
     */
	public void publish() {
		this.refreshNewsItem();
		// parentMidlet.displayInfo("Publishing....");
		// new Thread(new Runnable() {
		//
		// public void run() {
		try {

			final NewsItemPublisher publisher = new NewsItemPublisher(
					PublishPage.this.parentMidlet.preferences,
					new JavaMEWebHelper());

			final Vector validationErrors = publisher
					.publishNewsItem(PublishPage.this.parentMidlet.newsItem);
			if (validationErrors != null) {

				final StringBuffer validationError = new StringBuffer(
						"Validation Error!\n");
				String nextError = null;
				for (int i = 0; i < validationErrors.size(); i++) {
					nextError = (String) validationErrors.elementAt(i);
					validationError.append("\n");
					validationError.append(nextError);
				}
				this.parentMidlet.displayError(validationError.toString());
				// throw new RuntimeException("Validation Problems");
			} else {

				this.parentMidlet
						.displayInfo("News Item Published.\nNews Item URL:\n"
								+ this.parentMidlet.newsItem.getPostedURL());
				this.parentMidlet.resetNewsItem();
				this.parentMidlet.saveNewsItem();
				PublishPage.this.refreshForm();
			}

		} catch (final Exception e) {
			e.printStackTrace();
			this.parentMidlet.displayError(e.toString());
		}
		// }
		// }).start();

	}

	/**
     *
     */
	protected void refreshForm() {

		if (this.parentMidlet.newsItem.getSummary().length() > 99) {
			this.parentMidlet.newsItem.setSummary(this.parentMidlet.newsItem
					.getSummary().substring(0, 99));
		}

		if (this.parentMidlet.newsItem.getTitle().length() > 29) {
			this.parentMidlet.newsItem.setTitle(this.parentMidlet.newsItem
					.getTitle().substring(0, 29));
		}

		if (this.parentMidlet.newsItem.getAuthor().length() > 14) {
			this.parentMidlet.newsItem.setAuthor(this.parentMidlet.newsItem
					.getAuthor().substring(0, 14));
		}

		if (this.parentMidlet.newsItem.getText().length() > 99) {
			this.parentMidlet.newsItem.setText(this.parentMidlet.newsItem
					.getText().substring(0, 99));
		}

		this.titleField.setString(this.parentMidlet.newsItem.getTitle());
		this.authorField.setString(this.parentMidlet.newsItem.getAuthor());
		this.summaryField.setString(this.parentMidlet.newsItem.getSummary());
		this.textField.setString(this.parentMidlet.newsItem.getText());
		if (this.parentMidlet.newsItem.getAttachments().size() == 0) {
			this.attachmentsField.setText("No Attachments Selected");
		} else {
			final StringBuffer attachString = new StringBuffer(
					"Attachments Selected:");
			final Enumeration attachements = this.parentMidlet.newsItem
					.getAttachments().elements();
			int i = 1;
			String fileName = null;
			final JavaMEFileHelper fileHelper = new JavaMEFileHelper();
			while (attachements.hasMoreElements()) {
				attachString.append("\n" + i + ": ");
				fileName = fileHelper
						.getFilenameFromFilePath(((NewsItemAttachment) attachements
								.nextElement()).getFilePath());
				attachString.append(fileName);
				i++;
			}
			this.attachmentsField.setText(attachString.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.indybay.j2me.viewcontroller.IndyPublisherPage#refreshNewsItemFromForm
	 * ()
	 */
	protected void refreshNewsItem() {
		this.parentMidlet.newsItem.setTitle(this.titleField.getString());
		this.parentMidlet.newsItem.setAuthor(this.authorField.getString());
		this.parentMidlet.newsItem.setSummary(this.summaryField.getString());
		this.parentMidlet.newsItem.setText(this.textField.getString());

	}
}
