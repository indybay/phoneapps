/*
 PhotoDataSource class for for populating list of attachments on the Attachment list page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "PhotoDataSource.h"
#import "PhotoHelper.h"
#import "PhotoPageViewController.h"
#import "../../IndybayCppPublisher/src/persistence/filehelper.h"

@implementation PhotoDataSource

//init with link back to Photo Page View Controller
-(PhotoDataSource*)init:(PhotoPageViewController*)viewController
{
	self = [ super init ];
	photoPageController=viewController;
	[photoPageController retain];
	return self;
}


//Table view only has one section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
	return 1;
}


//return 1 to show a single row saying no attachments found if there are no attachments
//otherwise return the number of attachments
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int attachmentSize=[photoPageController.appDelegate newsItem]->getAttachments().size();
	return attachmentSize;
}


//display image for row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	vector<NewsItemAttachment> attachments=[photoPageController.appDelegate newsItem]->getAttachments();
	
	
	//create row if it is not yet created
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	}

		int rowIndex=[indexPath indexAtPosition:1];
		
		//get attachment for row
		NewsItemAttachment nextAttachment=attachments.at(rowIndex);
		//if thumbnail image was not yet created, created it
		NSString* nextLabel=[NSString stringWithFormat:@"# %d",rowIndex+1];
		if (nextAttachment.thumbnail==NULL){
			//create image from saved data
			
			NSString* filePath=[NSString stringWithUTF8String:nextAttachment.filePath.c_str()];
			UIImage* newImage=[UIImage imageWithContentsOfFile:filePath];
			
			PhotoHelper* photoHelper=[[PhotoHelper alloc] init];
			UIImage* newImage2=[photoHelper scaleAndRotateImage:newImage resizeAmount:ThumbnailSize originalOrientation:UIImageOrientationUp];
			
			[photoHelper release];
			
			[cell.imageView  setImage:newImage2];
			[photoPageController.appDelegate newsItem]->setThumbnailForAttachment(rowIndex,(void*)newImage2);
			[newImage2 retain];
		}else if ([cell.imageView image]!=nextAttachment.thumbnail){
			[cell.imageView  setImage:(UIImage*)nextAttachment.thumbnail];
		}
		
		[cell.textLabel setText:nextLabel];
		cell.showsReorderControl=YES;
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	return cell;
}


//we want to enable user sorting of images
- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;	
}

//reorder underlying array and save new ordernums when user reorders rows
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	int to=[toIndexPath indexAtPosition:1];
	int from=[fromIndexPath indexAtPosition:1];
	[photoPageController.appDelegate newsItem]->reorderAttachments(to,from);	
	[[photoPageController appDelegate ]saveCurrentNewsItem];
}

- (void)dealloc {
	[photoPageController release];
    [super dealloc];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		NewsItemAttachment selectedAttachment=[photoPageController.appDelegate newsItem]->getAttachments().at([indexPath indexAtPosition:1]);
		if (selectedAttachment.thumbnail!=NULL){
			[(UIImage*)selectedAttachment.thumbnail release];
			selectedAttachment.thumbnail=NULL;
		}
		[photoPageController.appDelegate newsItem]->removeAttachmentAtIndex([indexPath indexAtPosition:1]);
		[photoPageController.appDelegate setNeedsRefreshForAllViews];
		[photoPageController refreshFromCurrentItem];	
		 [[photoPageController appDelegate ] saveCurrentNewsItem];
	}
}

//display viw of single image 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	// Add UIImageView
	NewsItemAttachment selectedAttachment=[photoPageController.appDelegate newsItem]->getAttachments().at([indexPath indexAtPosition:1]);
	
	//((NewsItemAttachment*)[orderedAttachments objectAtIndex:[indexPath indexAtPosition:1]]);
	NSString* filePath=[NSString stringWithUTF8String:selectedAttachment.filePath.c_str()];
	UIImage* nextImage=[UIImage imageWithContentsOfFile:filePath];
	[photoPageController displayImage:nextImage];
}


@end
