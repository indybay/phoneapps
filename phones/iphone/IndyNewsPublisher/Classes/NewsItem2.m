//
//  NewsItem.m
//  IndyNewsPublisher
//
//  Created by zogren on 12/31/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "NewsItem.h"


@implementation NewsItem

/*
@synthesize title;
@synthesize author;
@synthesize summary;
@synthesize text;
@synthesize attachments;
@synthesize relatedLink;
@synthesize topicId;
@synthesize regionId;
@synthesize email;
@synthesize displayEmail;
*/
@dynamic title, author,summary,text,attachments,relatedLink,topicId,regionId,email,displayEmail;

/*
-(NewsItem* )init{
	[self loadFromPrefs];
	return self;
}

-(void)loadFromPrefs{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	NSString *savedTitle = [prefs stringForKey:@"title"];
	NSString *savedAuthor = [prefs stringForKey:@"author"];
	NSString *savedSummary = [prefs stringForKey:@"summary"];
	NSString *savedText = [prefs stringForKey:@"text"];
	NSString *savedLink = [prefs stringForKey:@"relatedLink"];
	NSString *savedEmail = [prefs stringForKey:@"email"];
	NSInteger savedTopicId = [prefs integerForKey:@"topicId"];
	NSInteger savedRegionId = [prefs integerForKey:@"regionId"];
	Boolean savedDisplayEmail = [prefs boolForKey:@"displayEmail"];
	
	
	if (savedTitle!=nil &&  [savedTitle length]>0){
		[self setTitle:savedTitle];
	}else{
		[self setTitle:@"Photos from protest"];
	}
	
	if (savedAuthor!=nil && [savedAuthor length]>0){
		[self setAuthor:savedAuthor];
		
	}else{
		[self setAuthor:@"Anonymous"];
	}
	
	if (savedSummary!=nil &&  [savedSummary length]>0){
		[self setSummary:savedSummary];
	}
	else{
		[self setSummary:@"Here are a few photos I took at the protest"];
	}
	
	if (savedText!=nil &&  [savedText length]>0){
		[self setText:savedText];
	}
	else{
		[self setText:@"Posted From My Phone"];
	}
	
	if (savedLink!=nil &&  [savedLink length]>0){
		[self setRelatedLink:savedLink];
	}
	else{
		[self setRelatedLink:@""];
	}
	
	
	if (savedEmail!=nil &&  [savedEmail length]>0){
		[self setEmail:savedEmail];
	}
	else{
		[self setEmail:@""];
	}
	
	[self setDisplayEmail:savedDisplayEmail];

	
	[self setTopicId:savedTopicId];
	[self setRegionId:savedRegionId];
	
	[self setAttachments:[[NSMutableArray alloc]init]];
	
}*/

-(void)save{
	/*NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

	[prefs setObject:self.title forKey:@"title"];

	[prefs setObject:self.author forKey:@"author"];

	[prefs setObject:self.summary forKey:@"summary"];

	[prefs setObject:self.text forKey:@"text"];
	
	[prefs setObject:self.relatedLink forKey:@"relatedLink"];
	
	[prefs setObject:self.email forKey:@"email"];
	
	[prefs setBool:self.displayEmail forKey:@"displayEmail"];
	
	[prefs setInteger:self.topicId forKey:@"topicId"];
	[prefs setInteger:self.regionId forKey:@"regionId"];
*/

}


@end



@implementation ImageToDataTransformer


+ (BOOL)allowsReverseTransformation {
	return YES;
}

+ (Class)transformedValueClass {
	return [NSData class];
}


- (id)transformedValue:(id)value {
	NSData *data = UIImagePNGRepresentation(value);
	return data;
}


- (id)reverseTransformedValue:(id)value {
	UIImage *uiImage = [[UIImage alloc] initWithData:value];
	return [uiImage autorelease];
}


@end
