/*
 PublishPageViewController class is view controller for the publish page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
#import "IndyNewsViewController.h"
#import "OptionList.h"


@interface PublishPageViewController : IndyNewsViewController <UIActionSheetDelegate, UIPickerViewDelegate> {

	IBOutlet UITextField *titleText;
	
	IBOutlet UITextView *summaryTextView;
	
	IBOutlet UIButton *dismissKeyboardButton;
	
	IBOutlet UIButton *clearKeyboardButton;
	
	IBOutlet UIButton *imageCount;
	
	IBOutlet UIButton *publishButton;
	
	IBOutlet UIButton *resetButton;
	
	IBOutlet UIActivityIndicatorView *activitySpinner;
	
	IBOutlet UIButton *regionButton;
	
	IBOutlet UIButton *topicButton;
	
	UIProgressView *progressBar;
	UILabel *progressLabel;
	
	UIActionSheet *publishActionSheet;
	
	UIPickerView *pickerView;
	
	OptionList* regionList;
	OptionList* topicList;
	NSObject* webController;
	
	NSMutableData* receivedData;
	bool isCurrentPickerRegion;
	
	bool actionSheetIsPublish;
	bool lastConnectSuccessful;
	bool isLoaded;
	int postStartTime;

}



-(IBAction)publishAction:(id)sender;
-(IBAction)switchToImageTab:(id)sender;
-(IBAction)hideKeyboardAction:(id)sender;
-(IBAction)choseRegion:(id)sender;
-(IBAction)choseTopic:(id)sender;
- (void) updateProgress;
-(void)startUpdatingProgress:(id)param;

- (IBAction)clearKeyboardAction:(id)sender;
-(void)indicateStartOfPublish;	
-(void)indicateStartOfConnect;
-(void)indicateEndOfPublish;	
-(void)indicateEndOfConnect;
-(void)indicateConnectionFailure:(NSString*)message;
-(void)indicateConnectionSuccess;
-(void)choseRegionOrTopic:(NSString*)selectionTitle;
-(void)indicatePublishSuccess:(NSString*)urlOfResult;
-(void)indicatePublishFailure:(NSString*)errorMessage;
-(void)resetPublishPage;
-(IBAction)resetNewsItem;

@end
