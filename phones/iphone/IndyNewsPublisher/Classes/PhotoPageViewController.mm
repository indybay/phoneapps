/*
 PhotoPageViewController class is view controller for the Attachment List page
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "PhotoPageViewController.h"
#import "PublishPageViewController.h"
#import "PhotoDataSource.h"
#import "PhotoHelper.h"
#import "MediaAttachmentViewController.h"
#import "filehelper.h"


@implementation PhotoPageViewController

-(PhotoPageViewController*) init{
	self=[super init];
	imagePickerView=nil;
	return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	NSLog(@"PhotoPageViewController loaded");
	PhotoDataSource* selectedImagesDataSource = [[PhotoDataSource alloc] init:self];
	imageTableView.dataSource=selectedImagesDataSource;
	imageTableView.delegate=selectedImagesDataSource;
	[imageTableView setRowHeight:105];
	needsRefresh=true;
}

- (IBAction)selectPhoto:(id)sender{
	[self setTableToNotEditMode:self];
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
		[self selectPhotoHelper:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
	}else {
		[self selectPhotoHelper:UIImagePickerControllerSourceTypePhotoLibrary];
	}
}


- (IBAction)setTableToEditMode:(id)sender{
	if (![imageTableView isEditing] && [appDelegate newsItem]->getAttachments().size()!=0){
		[imageTableView setEditing: YES animated: YES];
		[editButton setTitle:@"Done"];
	}else {
		[imageTableView setEditing: NO animated: YES];
		[editButton setTitle:@"Edit"];
	}
}


- (IBAction)setTableToNotEditMode:(id)sender{
	[imageTableView setEditing: NO animated: YES];
	[editButton setTitle:@"Edit"];
}


-(void)tabBarItemJustSelected{
	[self setTableToNotEditMode:self];
}



- (void)selectPhotoHelper:(int)source{
	if (imagePickerView==nil)
		imagePickerView = [[UIImagePickerController alloc] init];
	imagePickerView.delegate = self;
	[imagePickerView setSourceType:source];
	[self presentModalViewController:imagePickerView animated:YES];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)dictionary {
	
	PhotoResizeScale scale=MediumSize;
	switch ((int)round(resizeSlider.value)) {
		case 2:
			scale=SmallSize;
			break;
		case 3:
			scale=MediumSize;
			break;
		case 4:
			scale=LargeSize;
			break;
	}
	
	PhotoHelper* photoHelper=[[PhotoHelper alloc] init];
	UIImageOrientation originalOrientation = image.imageOrientation;
	UIImage* image2=[photoHelper scaleAndRotateImage:image 
										resizeAmount:scale
								 originalOrientation:originalOrientation];
	[photoHelper release];
	
	// Give a name to the file
	FileHelper fileHelper;
	string imageNameS=fileHelper.generateUniqueFileName(".png");
	NSString* imageName =[NSString stringWithUTF8String:imageNameS.c_str()];
	
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
	NSData* imageData =UIImagePNGRepresentation(image2);
	[imageData writeToFile:fullPathToFile atomically:YES];
	
	NewsItemAttachment newAttachment;
	newAttachment.filePath=[fullPathToFile UTF8String];
	newAttachment.thumbnail=NULL;
	[appDelegate newsItem]->addAttachment(newAttachment);
	[[self appDelegate] setNeedsRefreshForAllViews];
	
	[[self appDelegate ]saveCurrentNewsItem];
	[self refreshFromCurrentItem];
	
	[picker dismissModalViewControllerAnimated:YES];
	
}

- (void)displayImage:(UIImage*)image{
	
	MediaAttachmentViewController* mediaAttachmentViewController=[[MediaAttachmentViewController alloc]init];
	mediaAttachmentViewController.currentImage=image;
	
	[self presentModalViewController:mediaAttachmentViewController animated:YES];
	
}

-(void)refreshFromCurrentItem{
	
	if (needsRefresh){
		[super refreshFromCurrentItem];
		maxNumberAttachmentsLabel.text=[NSString stringWithFormat:@"(max %d)", [appDelegate preferences]->getMaxFilesAllowed()];
									
		NSNumber* resizeAmount=[NSNumber numberWithInt:[appDelegate preferences]->getResizeAmount()];
		if (resizeAmount==nil){
			resizeAmount=[NSNumber numberWithInt:2];
			[appDelegate preferences]->setResizeAmount([resizeAmount intValue]);
		}
		resizeSlider.value=[resizeAmount intValue];
		
		[imageTableView reloadData];
		if ([appDelegate newsItem]->getAttachments().size()!=0)
			[imageTableView setEditing: NO animated: YES];
		if ([appDelegate newsItem]->getAttachments().size()>=[appDelegate preferences]->getMaxFilesAllowed()){
			addButton.enabled=false;
		}else {
			addButton.enabled=true;
		}
	}
}

-(void)refreshCurrentItemFromView{
	[appDelegate preferences]->setResizeAmount(round(resizeSlider.value));
	[appDelegate savePreferences];
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	[imageTableView.dataSource release];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (IBAction)resizeSliderChanged:(id)sender{
	resizeSlider.value=round(resizeSlider.value);
	[self refreshCurrentItemFromView];
	
}


- (void)dealloc {
	if (imagePickerView!=nil)
		[imagePickerView release];
    [super dealloc];
}


@end
